# [aTag]lance Fitbit Versa Watchface

I wanted a watch face that had everything I needed in one place. All the data smushed into a single display. Everything you need, "at a glance."
## Getting Started

Clone and treat like any other Fitbit SDK watchface project

### Prerequisites

npm

## Authors

* **Stephen Tanner** - *Initial work* - [btreecat](https://gitlab.com/btreecat)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

Thank you to the Fitbit team for carrying the Pebble torch. 
