import {me} from "companion";
import {geolocation} from "geolocation";
import {settingsStorage} from "settings";
import {outbox} from "file-transfer";
import {encode} from "cbor";


const MILLISECONDS_PER_MINUTE = 1000 * 60;
const DATA_FILE = "data.cbor";
const MAX_AGE = 10 * MILLISECONDS_PER_MINUTE;
const TIMEOUT = 3 * MILLISECONDS_PER_MINUTE;
const EXPIRED = 10 * MILLISECONDS_PER_MINUTE;
const WAKE = 15 * MILLISECONDS_PER_MINUTE;

// // Wake companion app every 30 minutes
me.wakeInterval = WAKE;
// Monitor for significant changes in physical location
me.monitorSignificantLocationChanges = true;


//register listeners 
me.addEventListener("significantlocationchange", location_changed);
me.addEventListener("wakeinterval", get_recent_data);


//Who dare disturbs my slumber?
if (me.launchReasons.WokenUp) {
    // console.log("Companion App Woken Up");
    get_recent_data();
} else if (me.launchReasons.peerAppLaunched) {
    // The Device application caused the Companion to start
    // console.log("Device application was launched!");
    get_recent_data();
} else if (me.launchReasons.locationChanged) {
    // console.log("Significant Location Change");
    // console.log(JSON.stringify(me.launchReasons.locationChanged.coords));
    location_success(me.launchReasons.locationChanged);
} else if (me.launchReasons.settingsChanged) {
    // console.log("Settings Changed event");
    send_file({settings: build_settings()});
} else {
    // console.log("Companion App Launched For Unknown Reason");
    get_recent_data();
    //me.yield();
}


//Attempt to handle the event listener for the significant location change callback
function location_changed(evt) {
    console.log("Location change callback");
    console.log(JSON.stringify(evt));
}

//Start with getting the geo location, then reverse geocode, then get weather
//Send a JSON message back to the app
function get_fresh_data() {
    geolocation.getCurrentPosition(location_success, location_fail, {
        enableHighAccuracy: false,
        maximumAge: MAX_AGE,
        timeout: TIMEOUT
    });
}

//Successful geo lookup handler
function location_success(position) {
    // console.log("location_success() has been called");
    if (settingsStorage.getItem("ocg_api_key")) {
        let geo_json = fetch_geo_data(position.coords.latitude, position.coords.longitude, JSON.parse(settingsStorage.getItem("ocg_api_key")).name);
        geo_json.then(function (geo_response) {

            let sun_data = parse_sun(geo_response.results[0].annotations);
            let location = parse_location(geo_response.results[0].components);

            let data_local = {sun_rise: sun_data.sun_rise, sun_set: sun_data.sun_set, location: location};
            if (settingsStorage.getItem("ds_api_key")) {
                let weather_json = fetch_weather_data(position.coords.latitude, position.coords.longitude, JSON.parse(settingsStorage.getItem("ds_api_key")).name);
                weather_json.then(function (weather_response) {
                    // console.log("Weather Data Fetched");
                    //merge our data_local and are parses_weather data into one
                    let data = {...data_local, ...parse_weather(weather_response)};
                    let data_str = JSON.stringify({timestamp: Date.now(), data: data});
                    // console.log(data_str);
                    // cache the recent data
                    settingsStorage.setItem("CURRENT_DATA", data_str);
                    send_file({data: data, settings: build_settings()});
                });
            }

        })
    } else {
        //Missing one or both API keys from the user, don't try to fetch data yet
        console.log("API Keys Check Failed");

    }
}

//Failed geo lookup
function location_fail(error) {
    console.log("Geo Fix timeout error");
}


//Send the current data to the watch
function get_recent_data() {
    if (settingsStorage.getItem("CURRENT_DATA")) {
        let current_data = JSON.parse(settingsStorage.getItem("CURRENT_DATA"));
        let diff = Date.now() - current_data.timestamp;
        // console.log("Data Timestamp Diff: " + diff);
        if (diff > EXPIRED) {
            console.log("Data Expired");
            get_fresh_data();
        } else {
            //reuse data in storage
            console.log("Data Reused");
            let data = {data: current_data.data, settings: build_settings()};
            // console.log(JSON.stringify(data));
            send_file(data);
        }
    } else {
        get_fresh_data();
    }
}


//Take a JSON object and send it to the app as a file
function send_file(json_obj) {
    // const file_name = "api_data.cbor";
    // console.log("Sending File: " + JSON.stringify(json_obj));
    outbox.enqueue(DATA_FILE, encode(json_obj)).then(ft => {
        //console.log(`Transfer of '${DATA_FILE}' successfully queued.`);
    }).catch(function (error) {
        throw new Error(`Failed to enqueue '${DATA_FILE}'. Error: ${error}`);
    });
}


//Build the JSON object needed to send the settings to the app
function build_settings() {
    let settings = {};

    if (settingsStorage.getItem("text_color")) {
        settings.text_color = JSON.parse(settingsStorage.getItem("text_color"));
    }
    if (settingsStorage.getItem("background_color")) {
        settings.background_color = JSON.parse(settingsStorage.getItem("background_color"));
    }
    return settings;
}


//Parse the weather JSON data returned from the API fetch
function parse_weather(weather_json) {
    let data = {};

    data.condition = weather_json.currently.summary;
    data.wind_speed = Math.round(weather_json.currently.windSpeed);
    data.wind_direction = weather_json.currently.windBearing;
    data.temp = weather_json.currently.temperature;
    data.temp_high = Math.round(weather_json.daily.data[0].temperatureHigh);
    data.temp_low = Math.round(weather_json.daily.data[0].temperatureLow);

    return data;
}


//Parse out and properly format the sunrise and sunset data
function parse_sun(json) {
    let sun_rise, sun_set;

    //The reason I am using civil sunrise/sunset is because FAA drone regulations
    sun_rise = new Date((json.sun.rise.civil) * 1000);
    sun_set = new Date((json.sun.set.civil) * 1000);

    sun_rise = ("0" + sun_rise.getHours()).slice(-2) + ("0" + sun_rise.getMinutes()).slice(-2);
    sun_set = ("0" + sun_set.getHours()).slice(-2) + ("0" + sun_set.getMinutes()).slice(-2);

    return {sun_rise: sun_rise, sun_set: sun_set};

}


//Parse out the location name from the reverse geocode lookup
function parse_location(components) {
    //There are multiple possible values, I think this is the best ranking based on some exploration of the API but
    //it could require adjustments for the most helpful information being presented.
    let location = "";
    console.log("Location Type: " + components._type);
    if ("ficticious" in components){
        location = components.ficticious; //Where would there be a ficticious place?
    } else if ("village" in components) {
        location = components.village;
    } else if ("hamlet" in components) {
        location = components.hamlet;
    } else if ("neighbourhood" in components) {
        location = components.neighbourhood;
    } else if ("suburb" in components) {
        location = components.suburb;
    } else if ("town" in components) {
        location = components.town;
    } else if ("city" in components) {
        location = components.city;
    } else if ("county" in components) {
        location = components.county;
    } else if ("state_district" in components) {
        location = components.state_district;
    } else if ("state" in components) {
        location = components.state;
    } else if ("region" in components) {
        location = components.region;
    } else if ("island" in components) {
        location = components.island;
    } else if ("body_of_water" in components) {
        location = components.body_of_water;
    } else if ("country" in components) {
        location = components.country;
    } else if ("continent" in components) {
        location = components.continent;
    } else {
        //Always know where your towel is
        console.log("Somewhere: " + JSON.stringify(components));
        location = "Earth";
    }
    return location;
}


//Using lat and lng get the current location data from OpenCage Data
function fetch_geo_data(lat, lng, api_key) {
    //example
    //https://api.opencagedata.com/geocode/v1/json?q=LAT,LNG&key=<API KEY>&language=en&pretty=1

    let geo_url = "https://api.opencagedata.com/geocode/v1/json?q=" + lat + "," + lng + "&key=" + api_key + "&language=en&pretty=1";

    return fetch(geo_url)
        .then(function (response) {
            return response.json();
        });
}


//Using lat and lng get the current weather data from Dark Sky
function fetch_weather_data(lat, lng, api_key) {
    //example
    //https://api.darksky.net/forecast/<API KEY>/LAT,LNG

    //set units to ca to get km/h units
    let weather_url = "https://api.darksky.net/forecast/" + api_key + "/" + lat + "," + lng + "?units=ca";
    return fetch(weather_url)
        .then(function (response) {
            // console.log(response.status);
            return response.json();
        });
}