import document from "document";
import clock from "clock";
import {battery} from "power";
import {today} from "user-activity";
import {HeartRateSensor} from "heart-rate";
import {inbox} from "file-transfer";
import * as fs from "fs";

// let body = new BodyPresenceSensor();
// body.start();
let hrm = new HeartRateSensor();
hrm.start();

//Initialized values
//Set the current date to outside the range so we always update on the first tick
let current_day = 9;


clock.granularity = 'minutes'; // seconds, minutes, hours

update_battery();
update_hr();
inbox.addEventListener("newfile", read_data);
read_data();


//tick handler
clock.ontick = function (tick) {

    //Call these functions every minute (the tick granularity)
    update_colock(tick);
    update_steps();
    update_battery();

    //Runs once a day (at midnight or initial load)
    if (current_day != tick.date.getDay()) {
        update_dow(tick);
        update_date(tick);
        current_day = tick.date.getDay();
    }


};


//Take the color data from the settings and update the watch face
function update_settings(settings) {
    // console.log("Changing Colors: " + settings.text_color + " | " + settings.background_color );

    if (settings.bg_color !== "") {
        let bg = document.getElementById("background");
        bg.style.fill = settings.background_color;
    }

    if (settings.text_color !== "") {
        let elements = document.getElementsByTagName("text");
        elements.forEach(function (element) {
            element.style.fill = settings.text_color;
        });
    }
}


function update_hr() {
    hrm.onreading = function () {
        // console.log("HR Reading: " + hrm.heartRate);
        document.getElementById("heart_rate").text = hrm.heartRate;
    }
}


function read_data() {
    let file_name;
    while (file_name = inbox.nextFile()) {
        let data = fs.readFileSync(file_name, "cbor");
        consume_file(data);
    }
}


function consume_file(data) {
    // console.log(JSON.stringify(data));
    if (data.settings) {
        update_settings(data.settings);
    }
    if (data.data) {
        update_location_data(data.data);
    }
}


//Update the clock on the watch face
function update_colock(tick) {
    document.getElementById("clock_time").text = ("0" + tick.date.getHours()).slice(-2) + "|" +
        ("0" + tick.date.getMinutes()).slice(-2);
}


//write the updated data based on location info to screen
function update_location_data(data) {

    // console.log("location data updated");
    // console.log("Updating Data: " + data);

    document.getElementById("location").text = data.location;
    document.getElementById("sun_rise").text = data.sun_rise;
    document.getElementById("sun_set").text = data.sun_set;
    document.getElementById("condition").text = data.condition;
    document.getElementById("wind_speed").text = data.wind_speed + "km/h";
    document.getElementById("wind_direction").text = data.wind_direction + "&deg;";
    document.getElementById("temp").text = data.temp + "&deg;C";
    document.getElementById("temp_high").text = data.temp_high + "&deg;C";
    document.getElementById("temp_low").text = data.temp_low + "&deg;C";

}


//update the current step count for today
function update_steps() {
    document.getElementById("steps").text = (today.local.steps || 0);
}


//update charge level
function update_battery() {
    document.getElementById("charge_level").text = Math.floor(battery.chargeLevel) + "%";
}


//Update the day of week text
function update_dow(tick) {
    document.getElementById("day_of_week").text = day_map[tick.date.getDay()];
}

//update the date text
function update_date(tick) {
    document.getElementById("date").text = tick.date.getFullYear() + "." + ("0" + (tick.date.getMonth() + 1)).slice(-2) + "." + ("0" + (tick.date.getDate())).slice(-2);
}

//Map between the returned day of week and the string used to display it
let day_map = {
    0: "SUN",
    1: "MON",
    2: "TUE",
    3: "WED",
    4: "THR",
    5: "FRI",
    6: "SAT",
};