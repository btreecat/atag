function settingsComponent(props) {
    return (
        <Page>
            <Section
                title={<Text bold align="center">
                        [aTag]lance Settings
                    </Text>}>
            </Section>
            <Section>
                <Text>
                    Choose a text color:
                </Text>
                <ColorSelect
                    settingsKey="text_color"
                    colors={[
                        {color: '#f8bbd0'},
                        {color: '#d1c4e9'},
                        {color: '#bbdefb'},
                        {color: '#c8e6c9'},
                        {color: '#fff9c4'},
                        {color: '#ffe0b2'},
                        {color: '#ffccbc'},
                        {color: '#d7ccc8'},
                        {color: '#f44336'},
                        {color: '#e91e63'},
                        {color: '#9c27b0'},
                        {color: '#3f51b5'},
                        {color: '#03a9f4'},
                        {color: '#009688'},
                        {color: '#4caf50'},
                        {color: '#8bc34a'},
                        {color: '#ffeb3b'},
                        {color: '#ff9800'},
                        {color: '#ff5722'},
                        {color: '#795548'},
                        {color: '#b71c1c'},
                        {color: '#880e4f'},
                        {color: '#4a148c'},
                        {color: '#1a237e'},
                        {color: '#0d47a1'},
                        {color: '#004d40'},
                        {color: '#1b5e20'},
                        {color: '#f1c40f'},
                        {color: '#ff6f00'},
                        {color: '#bf360c'},
                        {color: '#ffffff'},
                        {color: '#eeeeee'},
                        {color: '#e0e0e0'},
                        {color: '#bdbdbd'},
                        {color: '#616161'},
                        {color: '#000000'},
                        {color: '#800000'},
                        {color: '#cc5500'}

                    ]}
                />
                <Text>
                    Choose a background color:
                </Text>
                <ColorSelect
                    settingsKey="background_color"
                    colors={[
                        {color: '#f8bbd0'},
                        {color: '#d1c4e9'},
                        {color: '#bbdefb'},
                        {color: '#c8e6c9'},
                        {color: '#fff9c4'},
                        {color: '#ffe0b2'},
                        {color: '#ffccbc'},
                        {color: '#d7ccc8'},
                        {color: '#f44336'},
                        {color: '#e91e63'},
                        {color: '#9c27b0'},
                        {color: '#3f51b5'},
                        {color: '#03a9f4'},
                        {color: '#009688'},
                        {color: '#4caf50'},
                        {color: '#8bc34a'},
                        {color: '#ffeb3b'},
                        {color: '#ff9800'},
                        {color: '#ff5722'},
                        {color: '#795548'},
                        {color: '#b71c1c'},
                        {color: '#880e4f'},
                        {color: '#4a148c'},
                        {color: '#1a237e'},
                        {color: '#0d47a1'},
                        {color: '#004d40'},
                        {color: '#1b5e20'},
                        {color: '#f1c40f'},
                        {color: '#ff6f00'},
                        {color: '#bf360c'},
                        {color: '#ffffff'},
                        {color: '#eeeeee'},
                        {color: '#e0e0e0'},
                        {color: '#bdbdbd'},
                        {color: '#616161'},
                        {color: '#000000'},
                        {color: '#800000'}, //Hokies Maroon
                        {color: '#cc5500'}, //and Burnt Orange
                    ]}
                />
                <Text>
                    API Keys:
                </Text>
                <TextInput
                    label="Dark Sky API Key"
                    placeholder = "https://darksky.net/"
                    settingsKey="ds_api_key"/>
                <TextInput
                    label="OpenCage Geocoder API Key"
                    placeholder = "https://opencagedata.com/"
                    settingsKey="ocg_api_key"/>

            </Section>
        </Page>
    );
}

registerSettingsPage(settingsComponent);
